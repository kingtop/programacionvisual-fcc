﻿namespace casaCambio
{
    partial class casa_cambio
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.cambiarMonedaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dolarAPesoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pesoADolarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.euroAPesoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pesoAEuroToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dolaAPesoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pesoADolarToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.button1 = new System.Windows.Forms.Button();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(141, 93);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(145, 22);
            this.textBox1.TabIndex = 0;
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cambiarMonedaToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(426, 28);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // cambiarMonedaToolStripMenuItem
            // 
            this.cambiarMonedaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.dolarAPesoToolStripMenuItem,
            this.euroAPesoToolStripMenuItem,
            this.dolaAPesoToolStripMenuItem});
            this.cambiarMonedaToolStripMenuItem.Name = "cambiarMonedaToolStripMenuItem";
            this.cambiarMonedaToolStripMenuItem.Size = new System.Drawing.Size(142, 24);
            this.cambiarMonedaToolStripMenuItem.Text = "Cambiar Moneda ";
            // 
            // dolarAPesoToolStripMenuItem
            // 
            this.dolarAPesoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.pesoADolarToolStripMenuItem});
            this.dolarAPesoToolStripMenuItem.Name = "dolarAPesoToolStripMenuItem";
            this.dolarAPesoToolStripMenuItem.Size = new System.Drawing.Size(224, 26);
            this.dolarAPesoToolStripMenuItem.Text = "Dolar a Peso";
            this.dolarAPesoToolStripMenuItem.Click += new System.EventHandler(this.dolarAPesoToolStripMenuItem_Click);
            // 
            // pesoADolarToolStripMenuItem
            // 
            this.pesoADolarToolStripMenuItem.Name = "pesoADolarToolStripMenuItem";
            this.pesoADolarToolStripMenuItem.Size = new System.Drawing.Size(224, 26);
            this.pesoADolarToolStripMenuItem.Text = "Peso a dolar ";
            this.pesoADolarToolStripMenuItem.Click += new System.EventHandler(this.pesoADolarToolStripMenuItem_Click);
            // 
            // euroAPesoToolStripMenuItem
            // 
            this.euroAPesoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.pesoAEuroToolStripMenuItem});
            this.euroAPesoToolStripMenuItem.Name = "euroAPesoToolStripMenuItem";
            this.euroAPesoToolStripMenuItem.Size = new System.Drawing.Size(224, 26);
            this.euroAPesoToolStripMenuItem.Text = "Euro a Peso";
            this.euroAPesoToolStripMenuItem.Click += new System.EventHandler(this.euroAPesoToolStripMenuItem_Click);
            // 
            // pesoAEuroToolStripMenuItem
            // 
            this.pesoAEuroToolStripMenuItem.Name = "pesoAEuroToolStripMenuItem";
            this.pesoAEuroToolStripMenuItem.Size = new System.Drawing.Size(224, 26);
            this.pesoAEuroToolStripMenuItem.Text = "Peso a Euro";
            this.pesoAEuroToolStripMenuItem.Click += new System.EventHandler(this.pesoAEuroToolStripMenuItem_Click);
            // 
            // dolaAPesoToolStripMenuItem
            // 
            this.dolaAPesoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.pesoADolarToolStripMenuItem1});
            this.dolaAPesoToolStripMenuItem.Name = "dolaAPesoToolStripMenuItem";
            this.dolaAPesoToolStripMenuItem.Size = new System.Drawing.Size(224, 26);
            this.dolaAPesoToolStripMenuItem.Text = "Dola a Euro";
            this.dolaAPesoToolStripMenuItem.Click += new System.EventHandler(this.dolaAPesoToolStripMenuItem_Click);
            // 
            // pesoADolarToolStripMenuItem1
            // 
            this.pesoADolarToolStripMenuItem1.Name = "pesoADolarToolStripMenuItem1";
            this.pesoADolarToolStripMenuItem1.Size = new System.Drawing.Size(224, 26);
            this.pesoADolarToolStripMenuItem1.Text = "Euro a dolar ";
            this.pesoADolarToolStripMenuItem1.Click += new System.EventHandler(this.pesoADolarToolStripMenuItem1_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3});
            this.dataGridView1.Location = new System.Drawing.Point(12, 229);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersWidth = 51;
            this.dataGridView1.RowTemplate.Height = 24;
            this.dataGridView1.Size = new System.Drawing.Size(402, 151);
            this.dataGridView1.TabIndex = 2;
            // 
            // Column1
            // 
            this.Column1.HeaderText = "Moneda";
            this.Column1.MinimumWidth = 6;
            this.Column1.Name = "Column1";
            // 
            // Column2
            // 
            this.Column2.HeaderText = "Cantidad";
            this.Column2.MinimumWidth = 6;
            this.Column2.Name = "Column2";
            // 
            // Column3
            // 
            this.Column3.HeaderText = "Resultado";
            this.Column3.MinimumWidth = 6;
            this.Column3.Name = "Column3";
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.RosyBrown;
            this.button1.Location = new System.Drawing.Point(340, 0);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(86, 31);
            this.button1.TabIndex = 3;
            this.button1.Text = "Exit";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // casa_cambio
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(426, 450);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.textBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "casa_cambio";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem cambiarMonedaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dolarAPesoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pesoADolarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem euroAPesoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pesoAEuroToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dolaAPesoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pesoADolarToolStripMenuItem1;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.Button button1;
    }
}

